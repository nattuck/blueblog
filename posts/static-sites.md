---
title: "Static Site Generators"
layout: default.liquid
is_draft: true
---

## Cobalt

 - custom with fswatch

## Jekyll

## Hugo

 - http://thecodestead.com/post/how-to-use-npm-as-a-build-tool-with-hugo/
 - https://gohugo.io/news/0.75.0-relnotes/

## Metalsmith
