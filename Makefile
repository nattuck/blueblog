
USER := nat
HOST := coyote.ferrus.net
RELP := ~/www

all:
	(cd _yarn && make)
	cobalt build

ship: all
	rsync -avz --delete _site/ $(USER)@$(HOST):$(RELP)

serve:
	cobalt serve

clean:
	rm -rf _site
	(cd _yarn && make clean)
